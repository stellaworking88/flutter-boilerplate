import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simas/widget/home/home_bloc.dart';
import 'package:simas/widget/home/home_event.dart';
import 'package:simas/widget/home/home_state.dart';

void main() {
  homeBloc();
}

void homeBloc() {
  group('HomeBloc Test', () {
    blocTest<HomeBloc, HomeState>(
      'emits [1] when increment is added',
      build: () => HomeBloc(),
      act: (bloc) => bloc.add(HomeEventIncrement()),
      expect: () => [HomeStateUpdateCount(1)],
    );
  });
}
